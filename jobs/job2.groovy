/* groovylint-disable-next-line CompileStatic */
String basePath = 'jobs/job2'
String repo = 'https://gitlab.com/moises.pequeno/dls-scripts.git'

folder(basePath) {
    description 'Just a folder for testing dsls'
}

pipelineJob("${basePath}/example-build") {
    // parameters {
    //     activeChoiceReactiveParam('CHOICE-1') {
    //         description('Allows user choose from multiple choices')
    //         filterable()
    //         choiceType('SINGLE_SELECT')
    //         scriptlerScript('Git Branch')
    //     }
    // }
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url ("${repo}")
                    }
                    branches ('master')
                }
            }
        }
        cps {
            script(readFileFromWorkspace('pipelines/node-pipeline2.groovy'))
        }
    }
}
