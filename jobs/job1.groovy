/* groovylint-disable-next-line CompileStatic */
String basePath = 'jobs/job1'
String repo = 'https://gitlab.com/moises.pequeno/dls-scripts.git'

folder(basePath) {
    description 'Just a folder for testing dsls'
}

pipelineJob("${basePath}/example-build") {
    triggers {
        scm '*/5 * * * *'
    }
   definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url ("${repo}")
                    }
                    branches ('master')
                }
            }
        }
        cps {
            script(readFileFromWorkspace('pipelines/node-pipeline.groovy'))
        }
   }
}
