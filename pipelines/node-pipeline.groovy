pipeline {
    agent { label 'agent-docker' }
    stages{
        stage('Checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/moises.pequeno/node-globant.git']]])
            }
        }
        stage('Build Image') {
            steps {
                script {
                  sh 'docker build --no-cache -t public.ecr.aws/n6i3t9l0/globant:latest .'
                }
            }
        }
        stage('Pushing to ECR') {
            steps{  
                
                 script {
                    sh 'aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/n6i3t9l0'
                    sh 'docker push public.ecr.aws/n6i3t9l0/globant:latest '
                 }
            }
        }
        stage('Deploy') {
            steps {
                
                script{
                    sh 'aws ecs update-service --cluster ecs-cluster --service ecs-service --force-new-deployment'
                }
            }
        }
       
    }
}
